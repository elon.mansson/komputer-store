//Global elements i will need in several diffrent functions.

const laptopsElement = document.getElementById("laptops");
const featureElement = document.getElementById("feature");
const imgElement = document.getElementById("img");
const laptopNameElement = document.getElementById("laptopName");
const laptopDescriptionElement = document.getElementById("laptopDescription");
const laptopPriceElement = document.getElementById("laptopPrice");
const bankElement = document.getElementById("balance");
const payElement = document.getElementById("pay");
const repayBtnElement = document.getElementById("btnRepay");
const loanElement = document.getElementById("loan");
const loanContainerElement = document.getElementById("loanContainer");

//Global varibels that i will need in several diffrent functions.

let laptops = [];
let bank = 0;
let pay = 0;
let currentLoan = 0;

//Button functions

//Button to be able to repay your debt. (Only showned if you have a current debt)
repayBtnElement.addEventListener("click", () => {
  currentLoan = currentLoan - pay;
  //Check if loan is 0 or less if it is it will remove repayBtn and loanInformation from the DOM. It will also update the current values
  if (currentLoan <= 0) {
    pay = -currentLoan;
    currentLoan = 0;
    repayBtnElement.style.display = "none";
    loanContainerElement.style.display = "none";
  } else {
    pay = 0
  }

  payElement.innerText = pay;
  loanElement.innerText = currentLoan;
});

//Button to buy computer.
const btnBuyComputerElement = document
  .getElementById("btnBuyComputer")
  .addEventListener("click", () => {
    const selectedLaptop = laptops[laptopsElement.selectedIndex];
    //Check if you have more money on the bank then the laptop price is if its true it will let you buy it.
    if (bank >= selectedLaptop.price) {
      bank = bank - selectedLaptop.price;
      bankElement.innerText = bank;
      alert(
        `Congratulation your the proud new owner of the ${selectedLaptop.title}`
      );
    } else {
      alert(
        "You don´t have enough money for the computer. You might need to work a bit."
      );
    }
  });

// A button to spam to get rich
const btnWorkElement = document
  .getElementById("btnWork")
  .addEventListener("click", () => {
    pay += 100;
    payElement.innerText = pay;
  });
// A button that will transfer your money to the bank.
const btnBankElement = document
  .getElementById("btnBank")
  .addEventListener("click", () => {
    //check if you have a loan if you do there is requierments to pay back a bit before you transfer.
    if (currentLoan == 0) {
      repayBtnElement.style.display = "none";

      bank += pay;
      pay = 0;
      payElement.innerText = pay;
      bankElement.innerText = bank;
    } else {
      let payAfterBills = pay * 0.9;
      bank += payAfterBills;
      currentLoan = currentLoan - pay * 0.1;
      pay = 0;
      if (currentLoan < 0) {
        bank = bank - currentLoan;
        currentLoan = 0;
      }
      loanElement.innerText = currentLoan;
      payElement.innerText = pay;
      bankElement.innerText = bank;
    }
  });

// A button so your will be able to take a loan.
const btnLoanElement = document
  .getElementById("btnLoan")
  .addEventListener("click", () => {
    //Check if you all ready have a loan and also controll that you dont take a loan higher then twice your bankbalance.
    if (currentLoan == 0) {
      const loan = prompt(
        "Please enter the amount you want to loan(cant exceed more then twice your bank balance): "
      );
      //Controll that you only send in numbers
      const checkIfNumber = ((/^[0-9]*$/).test(loan))
      if(checkIfNumber === true){
        if (loan > bank * 2) {
          alert(
            "You cant borrow more then twice the current amount you have on your bank balance"
          );
        } else {
          bank += parseInt(loan);
          currentLoan = loan;
          bankElement.innerText = bank;
          loanElement.innerText = currentLoan;
          repayBtnElement.style.display = "block";
          loanContainerElement.style.display = "flex";
        }
      } else {
        alert('You need to write numbers and only numbers!')
      }
    } else {
      alert("You have to pay back before you can borrow agien");
    }
  });

//Function to fetch the data from the API and also start the flow of the application to start setting up the DOM

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((res) => res.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToMenu(laptops));

//First step of break the API data into the DOM it will set all the DOM data to the first selected value without needing to change anything.
const addLaptopsToMenu = (laptops) => {
  //Start a function that will update the DOM
  addLaptopSpec(0);
  //Start a function for every laptop
  laptops.forEach((x) => addLaptopToMenu(x));
};

//Create all the options in the selector
const addLaptopToMenu = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopsElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
};

//Update all the information on the laptop
const addLaptopSpec = (a) => {
  //Empty the spec list if its not all ready empty
  featureElement.replaceChildren();
  //Check if any computer is selected else it will give it a hardcoded value.
  let selectedLaptop;
  if (a === 0) {
    selectedLaptop = laptops[0];
  } else {
    selectedLaptop = laptops[laptopsElement.selectedIndex];
  }
  //Loop through the laptop spec and display the spec in the DOM.
  selectedLaptop.specs.forEach((e) => {
    const specItem = document.createElement("li");
    specItem.innerText = e;
    featureElement.appendChild(specItem);
  });
  //Update the DOM with the remaning API data.
  if (selectedLaptop.id == 5){
    imgElement.src = `https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png`;
  } else {
    console.log(selectedLaptop.id)
    imgElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}`;
  }
  laptopNameElement.innerText = selectedLaptop.title;
  laptopDescriptionElement.innerText = selectedLaptop.description;
  laptopPriceElement.innerText = `${selectedLaptop.price} NOK`;
};

//Eventlisteners that isnt buttons.
laptopsElement.addEventListener("change", addLaptopSpec);
